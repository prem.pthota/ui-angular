import { Component, OnInit, AfterViewInit } from '@angular/core';

@Component({
	selector: 'app-footer',
	templateUrl: './footer.component.html',
	styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit, AfterViewInit {
	footerContainerCSSClasses: string;
	currentYear: string;
	constructor() {
		this.currentYear=''
		this.footerContainerCSSClasses=''
		
	 }

	ngOnInit(): void {
		const currentDate = new Date();
		this.currentYear = currentDate.getFullYear().toString();
	}
	ngAfterViewInit() {
	}

}
